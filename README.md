# Advent of Code 2021 in SQL

I decided to do the advent of code in SQL (PostgreSQL). The rules:

1. Try to avoid array datatypes
1. Don't use stored procedures or PL/SQL code. Only PostgreSQL-flavored SQL
1. Don't depend on anything else. Assume that there exists an `input` table, with an `input` field, that's the whole advent of code code only
