WITH RECURSIVE input as (select '5,3,2,2,1,1,4,1,5,5,1,3,1,5,1,2,1,4,1,2,1,2,1,4,2,4,1,5,1,3,5,4,3,3,1,4,1,3,4,4,1,5,4,3,3,2,5,1,1,3,1,4,3,2,2,3,1,3,1,3,1,5,3,5,1,3,1,4,2,1,4,1,5,5,5,2,4,2,1,4,1,3,5,5,1,4,1,1,4,2,2,1,3,1,1,1,1,3,4,1,4,1,1,1,4,4,4,1,3,1,3,4,1,4,1,2,2,2,5,4,1,3,1,2,1,4,1,4,5,2,4,5,4,1,2,1,4,2,2,2,1,3,5,2,5,1,1,4,5,4,3,2,4,1,5,2,2,5,1,4,1,5,1,3,5,1,2,1,1,1,5,4,4,5,1,1,1,4,1,3,3,5,5,1,5,2,1,1,3,1,1,3,2,3,4,4,1,5,5,3,2,1,1,1,4,3,1,3,3,1,1,2,2,1,2,2,2,1,1,5,1,2,2,5,2,4,1,1,2,4,1,2,3,4,1,2,1,2,4,2,1,1,5,3,1,4,4,4,1,5,2,3,4,4,1,5,1,2,2,4,1,1,2,1,1,1,1,5,1,3,3,1,1,1,1,4,1,2,2,5,1,2,1,3,4,1,3,4,3,3,1,1,5,5,5,2,4,3,1,4' as input)
  , initial AS (
    SELECT 0 AS day, regexp_split_to_table(input, ',')::integer as fish
    FROM input
  )
  , grouped AS (
    SELECT day, fish, COUNT(fish) n FROM initial GROUP BY day, fish
  )
  , fishes AS (
    SELECT day, fish, n :: bigint FROM grouped
    UNION ALL
    (with f as (TABLE fishes)
      SELECT a.day, a.fish, SUM(a.n)  :: bigint
      FROM (
        SELECT day+1 AS day, fish-1 AS fish, n FROM f WHERE fish > 0
        UNION ALL
        SELECT day+1, unnest('{6, 8}'::integer[]), n FROM f WHERE fish = 0
      ) a WHERE a.day <= 256 GROUP BY a.day, a.fish
    )
  )

SELECT SUM(n) res FROM fishes WHERE day = 80 OR day = 256 GROUP BY day;
