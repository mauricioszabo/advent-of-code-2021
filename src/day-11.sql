WITH RECURSIVE
  input(input) AS (SELECT '1553421288
5255384882
1224315732
4258242274
1658564216
6872651182
5775552238
5622545172
8766672318
2178374835')
  , octopi AS (
    SELECT row, col, energy :: bigint
    FROM regexp_split_to_table((SELECT input FROM input), '\n') WITH ORDINALITY AS r(c, row)
    CROSS JOIN regexp_split_to_table(c, '') WITH ORDINALITY AS c(energy, col)
  )
  , steps AS (
    SELECT row, col, energy, 0 AS step, 0 AS substep FROM octopi
    UNION ALL
    (
      WITH r_steps AS (TABLE steps)
           , vicinity AS (
              SELECT s.row, s.col, COUNT(*) AS increment, 0 AS step_inc
              FROM r_steps s
              INNER JOIN r_steps AS s2
                ON s2.energy = 10
                AND s.row BETWEEN s2.row-1 AND s2.row+1
                AND s.col BETWEEN s2.col-1 AND s2.col+1
              GROUP BY s.row, s.col, s.energy
           )
      SELECT s.row, s.col,
        CASE
        WHEN v.increment IS NULL
          THEN s.energy
        WHEN s.energy < 10 AND s.energy+v.increment > 10
          THEN 10
        WHEN s.energy > 9 AND v.step_inc = 1
          THEN v.increment
        ELSE
          s.energy+v.increment
        END,
        s.step+COALESCE(v.step_inc, 0),
        s.substep+1
      FROM r_steps AS s
      LEFT JOIN (
        SELECT * FROM vicinity
        UNION
        SELECT row, col, 1 AS increment, 1 AS step_inc
        FROM GENERATE_SERIES(1, (SELECT MAX(col) FROM octopi)) col
        CROSS JOIN GENERATE_SERIES(1, (SELECT MAX(row) FROM octopi)) row
        WHERE NOT EXISTS(SELECT * FROM vicinity)
      ) v
        ON v.row = s.row AND v.col = s.col
      WHERE s.step < 1001
    )
  )
  , energy_levels AS (
    SELECT DISTINCT s.row, s.col, CASE
    WHEN s.energy >= 10 THEN 0
    ELSE s.energy
    END energy
    , s.step
    FROM steps s
    INNER JOIN (
      SELECT row, col, step, MAX(substep) substep
      FROM steps
      GROUP BY row, col, step
    ) s2 ON s.row=s2.row AND s.col=s2.col AND s.step=s2.step AND s.substep=s2.substep
  )

  SELECT COUNT(energy)
  FROM energy_levels
  WHERE step BETWEEN 1 AND 100 AND energy = 0
  UNION ALL
  SELECT MIN(step)
  FROM (
    SELECT step
    FROM energy_levels
    GROUP BY step
    HAVING SUM(energy) = 0
  ) s
  ;
