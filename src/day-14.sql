WITH RECURSIVE input(input) AS (SELECT 'HBCHSNFFVOBNOFHFOBNO

HF -> O
KF -> F
NK -> F
BN -> O
OH -> H
VC -> F
PK -> B
SO -> B
PP -> H
KO -> F
VN -> S
OS -> B
NP -> C
OV -> C
CS -> P
BH -> P
SS -> P
BB -> H
PH -> V
HN -> F
KV -> H
HC -> B
BC -> P
CK -> P
PS -> O
SH -> N
FH -> N
NN -> P
HS -> O
CB -> F
HH -> F
SB -> P
NB -> F
BO -> V
PN -> H
VP -> B
SC -> C
HB -> H
FP -> O
FC -> H
KP -> B
FB -> B
VK -> F
CV -> P
VF -> V
SP -> K
CC -> K
HV -> P
NC -> N
VH -> K
PF -> P
PB -> S
BF -> K
FF -> C
FV -> V
KS -> H
VB -> F
SV -> F
HO -> B
FN -> C
SN -> F
OB -> N
KN -> P
BV -> H
ON -> N
NF -> S
OF -> P
NV -> S
VS -> C
OO -> C
BP -> H
BK -> N
CP -> N
PC -> K
CN -> H
KB -> B
BS -> P
KK -> P
SF -> V
CO -> V
CH -> P
FO -> B
FS -> F
VO -> H
NS -> F
KC -> H
VV -> K
NO -> P
OK -> F
PO -> V
FK -> H
OP -> H
PV -> N
CF -> P
NH -> K
SK -> O
KH -> P
HP -> V
OC -> V
HK -> F')
  , temp(input) AS (SELECT regexp_split_to_table(input, '\n\n') FROM input)
  , template(pair) AS (
    SELECT STRING_AGG(elem, '') OVER (ROWS 1 PRECEDING)
    FROM (SELECT * FROM temp LIMIT 1) s
    CROSS JOIN regexp_split_to_table(input, '') WITH ORDINALITY AS r(elem, idx)
    OFFSET 1
  )
  , rules(pair, insertion) AS (
    SELECT regexp_replace(row, ' ->.*', ''), regexp_replace(row, '.*-> ', '')
    FROM (SELECT * FROM temp OFFSET 1) s
    CROSS JOIN regexp_split_to_table(input, '\n') AS r(row))
  , pairs(pair, c) AS (
    SELECT pair, COUNT(pair) FROM template GROUP BY pair
  )
  , steps(step, pair, c) AS (
    SELECT 0, pair, c FROM pairs
    UNION ALL
    SELECT step, pair, SUM(c) :: bigint FROM (
      SELECT step+1 step, regexp_split_to_table(
        regexp_replace(steps.pair, '(.)', '\1' || insertion || '|' || insertion)
        , '\|'
      ) pair, c  :: bigint
      FROM steps
      INNER JOIN rules ON rules.pair = steps.pair
      WHERE step <= 40
    ) s GROUP BY step, pair
  )
  , frequencies AS (
    SELECT step, letter, SUM(c) as sum
    FROM (
      SELECT step, regexp_replace(pair, '.$', '') letter, c FROM steps
      UNION ALL
      SELECT i.step, regexp_replace(s.input, '.*(.)', '\1'), 1
      FROM (SELECT * FROM temp LIMIT 1) s, GENERATE_SERIES(1, 40) AS i(step)
    ) s
    GROUP BY step, letter
  )
  , at_10 AS (SELECT * FROM frequencies WHERE step = 10)
  , at_40 AS (SELECT * FROM frequencies WHERE step = 40)

  SELECT max.sum - min.sum
  FROM
    (SELECT * FROM at_10 ORDER BY sum DESC LIMIT 1) max,
    (SELECT * FROM at_10 ORDER BY sum LIMIT 1) min
  UNION ALL
  SELECT max.sum - min.sum
  FROM
    (SELECT * FROM at_40 ORDER BY sum DESC LIMIT 1) max,
    (SELECT * FROM at_40 ORDER BY sum LIMIT 1) min
  ;
